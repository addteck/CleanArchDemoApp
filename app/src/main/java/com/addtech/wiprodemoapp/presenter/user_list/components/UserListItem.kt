package com.addtech.wiprodemoapp.presenter.user_list.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.presenter.Navigation.Screen


@Composable
fun UserListItem(
    user: User,
    onItemClick: (Screen,Int) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onItemClick(Screen.UserDetailScreen,user.id) }
            .padding(20.dp),
    ) {
        Image(
            painter = rememberAsyncImagePainter(user.avatar),
            contentDescription = null,
            modifier = Modifier.size(100.dp).clickable {
                onItemClick(Screen.UserImageScreen,user.id)
            }
        )
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            fontSize = 18.sp,
            text = "${user.firstName} ${user.lastName} ",
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier.clickable {
                onItemClick(Screen.UserTextScreen,user.id)
            }

        )

    }
}