package com.addtech.wiprodemoapp.presenter.Navigation

sealed class Screen(val route: String) {
    data object UserListScreen: Screen("user_list_screen")
    data object UserDetailScreen: Screen("user_detail_screen")
    data object UserImageScreen: Screen("user_image")
    data object UserTextScreen: Screen("user_text")
}
