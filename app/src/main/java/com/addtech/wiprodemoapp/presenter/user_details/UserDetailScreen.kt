package com.addtech.wiprodemoapp.presenter.user_details

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.addtech.wiprodemoapp.presenter.ui.UiState

@Composable
fun UserDetailsScreen(
    viewModel: UserDetailViewModel = hiltViewModel()
) {
    val uiState = viewModel.uiState.value

    Box(modifier = Modifier.fillMaxSize()) {
        when(uiState){
            is UiState.Success -> {
               val user = uiState.data
                user?.let {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(20.dp),
                    ) {
                        Image(
                            painter = rememberAsyncImagePainter(user.avatar),
                            contentDescription = null,
                            modifier = Modifier.size(100.dp).testTag(tag = "user_image")
                        )
                        Spacer(modifier = Modifier.width(10.dp))
                        Text(
                            fontSize = 20.sp,
                            text = "${user.firstName} ${user.lastName} ",
                            style = MaterialTheme.typography.labelLarge,
                            modifier = Modifier.testTag("user_details")
                        )

                    }
                }

            }
            is UiState.Error -> {
                Text(
                    text = uiState.message,
                    color = MaterialTheme.colorScheme.error,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .align(Alignment.Center)
                )
            }
            UiState.Loading ->  CircularProgressIndicator(modifier = Modifier.align(Alignment.Center)
                .testTag("user_loader"))
        }

    }

}