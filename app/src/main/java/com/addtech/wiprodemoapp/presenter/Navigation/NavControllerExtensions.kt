package com.addtech.wiprodemoapp.presenter.Navigation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.core.net.toUri
import androidx.navigation.NavController
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavDestination.Companion.createRoute

@SuppressLint("RestrictedApi")
fun NavController.navigateWithArgs(
    route: String,
    args: Bundle,
) {
    val routeLink = NavDeepLinkRequest.Builder.fromUri(createRoute(route).toUri()).build()

    val deeplinkMatch = graph.matchDeepLink(routeLink)
    deeplinkMatch?.let {
        navigate(it.destination.id, args)
    } ?: navigate(route)
}
