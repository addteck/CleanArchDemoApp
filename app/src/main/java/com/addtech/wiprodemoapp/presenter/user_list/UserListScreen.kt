package com.addtech.wiprodemoapp.presenter.user_list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.addtech.wiprodemoapp.presenter.Navigation.Screen
import com.addtech.wiprodemoapp.presenter.ui.UiState
import com.addtech.wiprodemoapp.presenter.user_list.components.UserListItem

@Composable
fun UserListScreen(
    viewModel: UserListViewModel = hiltViewModel(),
    userItemClicked: (Screen, String) -> Unit
) {
    when (val states = viewModel.state.value) {
        is UiState.Success -> {
            Box(modifier = Modifier.fillMaxSize()) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("user_list")
                ) {
                    items(states.data!!) { user ->
                        UserListItem(
                            user = user,
                            onItemClick = { screen, id ->
                                userItemClicked(screen, id.toString())
                            }
                        )
                    }
                }
            }
        }

        is UiState.Error -> Box(modifier = Modifier.fillMaxSize()) {
            Text(
                text = states.message,
                color = MaterialTheme.colorScheme.error,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
            )
        }

        is UiState.Loading -> Box(modifier = Modifier.fillMaxSize()) {
            CircularProgressIndicator(
                modifier = Modifier
                    .align(Alignment.Center)
                    .testTag("progress")
            )
        }
    }
}