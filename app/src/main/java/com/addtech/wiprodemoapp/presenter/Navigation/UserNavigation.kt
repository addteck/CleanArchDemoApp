
package com.addtech.wiprodemoapp.presenter.Navigation

import androidx.compose.runtime.Composable
import androidx.core.os.bundleOf
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.addtech.wiprodemoapp.core.common.Constants
import com.addtech.wiprodemoapp.presenter.user_details.UserDetailsScreen
import com.addtech.wiprodemoapp.presenter.user_details.UserImageScreen
import com.addtech.wiprodemoapp.presenter.user_details.UserTextScreen
import com.addtech.wiprodemoapp.presenter.user_list.UserListScreen

@Composable
fun UserNavigation(navController: NavHostController) {

    NavHost(
        navController = navController,
        startDestination = Screen.UserListScreen.route
    ) {
        composable(
            route = Screen.UserListScreen.route
        ) {
            UserListScreen{
                screen,userId->
                //we can check screen and create bundle args
                /* ex-
                when(screen){
                    is Screen.UserDetailScreen-> {
                        val args = bundleOf(
                            Constants.PARAM_USER_ID to userId
                        )
                    }
                }*/

                val args = bundleOf(
                    Constants.PARAM_USER_ID to userId
                )
                navController.navigateWithArgs(screen.route, args = args)
            }
        }
        composable(
            route = Screen.UserDetailScreen.route
        ) {
            UserDetailsScreen()
        }
        composable(
            route = Screen.UserImageScreen.route
        ) {
            UserImageScreen()
        }
        composable(
            route = Screen.UserTextScreen.route
        ) {
            UserTextScreen()
        }
    }
}
