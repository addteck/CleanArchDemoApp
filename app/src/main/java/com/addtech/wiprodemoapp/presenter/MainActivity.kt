package com.addtech.wiprodemoapp.presenter

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.addtech.wiprodemoapp.presenter.Navigation.Screen
import com.addtech.wiprodemoapp.presenter.Navigation.UserNavigation
import com.addtech.wiprodemoapp.presenter.ui.theme.WiproDemoAppTheme
import com.addtech.wiprodemoapp.presenter.user_details.UserDetailsScreen
import com.addtech.wiprodemoapp.presenter.user_list.UserListScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WiproDemoAppTheme {
                // A surface container using the 'background' color from the theme
               App()
            }
        }
    }


    @Composable
    fun App() {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ){
            val navHostController = rememberNavController()
            UserNavigation(navHostController)
        }
    }
}
