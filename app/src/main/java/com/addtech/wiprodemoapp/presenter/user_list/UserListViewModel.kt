package com.addtech.wiprodemoapp.presenter.user_list

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.domain.usecase.GetUsersUseCase
import com.addtech.wiprodemoapp.presenter.ui.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class UserListViewModel @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase
) : ViewModel() {

    private val _state: MutableState<UiState<List<User>>> = mutableStateOf(UiState.Loading)
    val state: State<UiState<List<User>>> = _state

    init {
        getUsers()
    }

    private fun getUsers() {
        getUsersUseCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = UiState.Success(data = result.data ?: emptyList())
                }

                is Resource.Error -> {
                    _state.value = UiState.Error(
                        message = result.message ?: "An unexpected error occured"
                    )
                }

                is Resource.Loading -> {
                    _state.value = UiState.Loading
                }
            }
        }.launchIn(viewModelScope)
    }
}