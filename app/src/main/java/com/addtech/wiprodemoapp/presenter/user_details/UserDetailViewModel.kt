package com.addtech.wiprodemoapp.presenter.user_details

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.addtech.wiprodemoapp.core.common.Constants
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.domain.usecase.GetUserDetailsUseCase
import com.addtech.wiprodemoapp.presenter.ui.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class UserDetailViewModel @Inject constructor(
    private val getUserDetailsUseCase: GetUserDetailsUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _uiState:MutableState<UiState<User>> = mutableStateOf(UiState.Loading)
    val uiState: State<UiState<User>> = _uiState

    init {
        savedStateHandle.get<String>(Constants.PARAM_USER_ID)?.let { id ->
            getUserDetails(id)
        }
    }

    private fun getUserDetails(id: String) {
        getUserDetailsUseCase(id).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _uiState.value= UiState.Success(result.data)
                }

                is Resource.Error -> {
                    _uiState.value = UiState.Error(result.message ?: "An unexpected error occured")
                }

                is Resource.Loading -> {
                    _uiState.value= UiState.Loading
                }
            }
        }.launchIn(viewModelScope)
    }
}