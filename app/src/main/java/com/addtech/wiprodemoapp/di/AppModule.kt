package com.addtech.wiprodemoapp.di

import com.addtech.wiprodemoapp.data.remote.UserApi
import com.addtech.wiprodemoapp.core.common.Constants
import com.addtech.wiprodemoapp.data.repository.UserRepoImpl
import com.addtech.wiprodemoapp.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideUserApi(): UserApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(UserApi::class.java)
    }

    @Provides
    fun provideUserRepository(api: UserApi): UserRepository {
        return UserRepoImpl(api)
    }
}