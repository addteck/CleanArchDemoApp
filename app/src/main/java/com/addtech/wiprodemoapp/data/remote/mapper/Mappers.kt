package com.addtech.wiprodemoapp.data.remote.mapper

import com.addtech.wiprodemoapp.data.remote.dto.UserDto
import com.addtech.wiprodemoapp.domain.model.User


fun UserDto.toDomain() = User(
    avatar = avatar,
    firstName = firstName,
    id = id,
    lastName = lastName
)