package com.addtech.wiprodemoapp.data.remote.dto

data class Support(
    val text: String,
    val url: String
)