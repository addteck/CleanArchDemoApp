// YApi QuickType插件生成，具体参考文档:https://plugins.jetbrains.com/plugin/18847-yapi-quicktype/documentation

package com.addtech.wiprodemoapp.data.remote.dto

data class UserDetailDto (
    val data: UserDto,
    val support: Support
)

