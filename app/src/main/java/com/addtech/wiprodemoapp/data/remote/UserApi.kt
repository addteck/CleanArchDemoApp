package com.addtech.wiprodemoapp.data.remote

import com.addtech.wiprodemoapp.data.remote.dto.UserDetailDto
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UserApi {
    @GET("api/users")
    suspend fun getUsers(): Response<UserResponse>

    @GET("api/users/{id}")
    suspend fun getUserDetails(@Path("id") userId: String): Response<UserDetailDto>


}