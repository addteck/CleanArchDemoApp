package com.addtech.wiprodemoapp.data.repository

import com.addtech.wiprodemoapp.core.utils.BaseRepository
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.data.remote.UserApi
import com.addtech.wiprodemoapp.data.remote.dto.UserDetailDto
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse
import com.addtech.wiprodemoapp.domain.repository.UserRepository
import javax.inject.Inject

class UserRepoImpl @Inject constructor(private val userApi: UserApi) : BaseRepository(), UserRepository {
    override suspend fun getUsers(): Resource<UserResponse> {
        return safeCallApi { userApi.getUsers() }
    }

    override suspend fun getUser(id: String): Resource<UserDetailDto> {
        return safeCallApi { userApi.getUserDetails(id) }
    }
}