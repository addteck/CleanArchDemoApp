package com.addtech.wiprodemoapp.data.remote.dto

import com.addtech.wiprodemoapp.domain.model.User
import com.google.gson.annotations.SerializedName


data class UserDto(
    val avatar: String,
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    val id: Int,
    @SerializedName("last_name")
    val lastName: String
)

