package com.addtech.wiprodemoapp.data.remote.dto

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("data")
    val users: List<UserDto>,
    val page: Int,
    @SerializedName("per_page")
    val perPage: Int,
    val support: Support,
    val total: Int,
    @SerializedName("total_pages")
    val totalPages: Int
)