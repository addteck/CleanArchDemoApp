package com.addtech.wiprodemoapp.core.common

object Constants {

    const val BASE_URL = "https://reqres.in/"
    const val PARAM_USER_ID = "id"

}