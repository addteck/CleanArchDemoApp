package com.addtech.wiprodemoapp.core.utils

import retrofit2.HttpException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.IOException
import java.lang.Exception

abstract class BaseRepository {
    suspend fun <T> safeCallApi(apiToBeCalled: suspend () -> Response<T>): Resource<T> {
        return withContext(Dispatchers.IO) {
            try {
                val response = apiToBeCalled()
                if (response.isSuccessful) {
                    Resource.Success(data = response.body() as T)
                } else {
                    Resource.Error(response.errorBody()?.string() ?: "An unexpected error occured")
                }
            } catch (e: HttpException) {
                Resource.Error(e.localizedMessage ?: "An unexpected error occured")
            } catch (e: IOException) {
                Resource.Error("Couldn't reach server. Check your internet connection.")
            } catch (e: Exception) {
                Resource.Error(e.message ?: "Something went wrong")
            }
        }
    }
}