package com.addtech.wiprodemoapp.domain.repository

import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.data.remote.dto.UserDetailDto
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse

interface UserRepository {

    suspend fun getUsers(): Resource<UserResponse>

    suspend fun getUser(id:String): Resource<UserDetailDto>
}