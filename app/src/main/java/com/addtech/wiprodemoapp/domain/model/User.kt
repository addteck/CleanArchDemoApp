package com.addtech.wiprodemoapp.domain.model


data class User(
    val avatar: String,
    val firstName: String,
    val id: Int,
    val lastName: String
)
