package com.addtech.wiprodemoapp.domain.usecase

import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse
import com.addtech.wiprodemoapp.data.remote.mapper.toDomain

import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetUsersUseCase @Inject constructor(private val userRepository: UserRepository) {

    operator fun invoke(): Flow<Resource<List<User>>> = flow {

        emit(Resource.Loading())
        when (val userResponse = userRepository.getUsers()) {
            is Resource.Success -> {
                val users: List<User> =
                    (userResponse.data as UserResponse).users.map { it.toDomain() }
                emit(Resource.Success(users))
            }

            is Resource.Error -> emit(
                Resource.Error(userResponse.message ?: "something went wrong")
            )

            is Resource.Loading -> emit(Resource.Loading())
        }

    }
}