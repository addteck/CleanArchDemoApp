package com.addtech.wiprodemoapp.presenter.user_list

import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.domain.usecase.GetUsersUseCase
import com.addtech.wiprodemoapp.presenter.ui.UiState
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class UserListViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var getUsersUseCase: GetUsersUseCase


    @Before
    fun setup() {
        getUsersUseCase = mock()

    }

    @Test
    fun givenResponseLoadingWhenGetUsersUseCaseCalled() = runTest {

        `when`(getUsersUseCase()).thenReturn(
            flow {
                emit(Resource.Loading())
            }
        )
        val userListViewModel = UserListViewModel(getUsersUseCase)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Loading, userListViewModel.state.value)
    }

    @Test
    fun givenResponseSuccessWhenGetUsersUseCaseCalled() = runTest {

        `when`(getUsersUseCase()).thenReturn(
            flow {
                emit(
                    Resource.Success(
                        data = listOf(
                            User(
                                avatar = "image_url",
                                firstName = "amit",
                                id = 1,
                                lastName = "Den"
                            )
                        )
                    )
                )
            }
        )
        val userListViewModel = UserListViewModel(getUsersUseCase)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Success(listOf(
            User(
                avatar = "image_url",
                firstName = "amit",
                id = 1,
                lastName = "Den"
            )
        )), userListViewModel.state.value)
    }


    @Test
    fun givenResponseFailureWhenGetUsersUseCaseCalled() = runTest {

        `when`(getUsersUseCase()).thenReturn(
            flow {
                emit(Resource.Error(message = "something went wrong"))
            }
        )
        val userListViewModel = UserListViewModel(getUsersUseCase)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Error("something went wrong"), userListViewModel.state.value)
    }

}