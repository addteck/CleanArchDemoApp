package com.addtech.wiprodemoapp.presenter.user_details

import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.data.remote.dto.Support
import com.addtech.wiprodemoapp.data.remote.dto.UserDetailDto
import com.addtech.wiprodemoapp.data.remote.dto.UserDto
import com.addtech.wiprodemoapp.domain.repository.UserRepository
import com.addtech.wiprodemoapp.domain.usecase.GetUserDetailsUseCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner.Silent

@ExperimentalCoroutinesApi
@RunWith(Silent::class)
class GetUserDetailsUseCaseTest {

    @get:Rule
    val mainCoroutineRule = MainDispatcherRule()

    private lateinit var userRepository: UserRepository
    private lateinit var getUserDetailsUseCase: GetUserDetailsUseCase

    @Before
    fun setUp() {
        userRepository = mock()
        getUserDetailsUseCase = GetUserDetailsUseCase(userRepository)
    }


    @Test
    fun givenUserDetailsSuccessFromUseCase() = runTest {
        `when`(userRepository.getUser("1")).thenReturn(
            Resource.Success(
                data = UserDetailDto(
                    data = UserDto(
                        avatar = "image_url",
                        firstName = "amit",
                        id = 1,
                        lastName = "Den",
                        email = "test.com"
                    ),
                    support = Support("tes", "url")

                )
            )
        )

        getUserDetailsUseCase("1").onEach {
            assertEquals("amit", it.data!!.firstName)
        }
    }

    @Test
    fun returnErrorFromGetUserDetailsUseCase() = runTest {
        `when`(userRepository.getUser("1")).thenThrow(
            RuntimeException("Something Went Wrong")
        )
        getUserDetailsUseCase("1").onEach {
            assertEquals("Something Went Wrong", it.message)
        }
    }
}