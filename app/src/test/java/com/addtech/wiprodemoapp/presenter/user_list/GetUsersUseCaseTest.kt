package com.addtech.wiprodemoapp.presenter.user_list

import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.data.remote.dto.Support
import com.addtech.wiprodemoapp.data.remote.dto.UserDto
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse
import com.addtech.wiprodemoapp.domain.repository.UserRepository
import com.addtech.wiprodemoapp.domain.usecase.GetUsersUseCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner.Silent

@ExperimentalCoroutinesApi
@RunWith(Silent::class)
class GetUsersUseCaseTest {

    @get:Rule
    val mainCoroutineRule = MainDispatcherRule()

    private lateinit var userRepository: UserRepository
    private lateinit var getUsersUseCase: GetUsersUseCase

    @Before
    fun setUp() {
        userRepository = mock()
        getUsersUseCase = GetUsersUseCase(userRepository)
    }


    @Test
    fun givenUsersSuccessFromUseCase() = runTest {
        `when`(userRepository.getUsers()).thenReturn(
            Resource.Success(
                data = UserResponse(
                    users= listOf(UserDto(
                        avatar = "image_url",
                        firstName = "amit",
                        id = 1,
                        lastName = "Den",
                        email = "test.com"
                    )),
                page=1,
                    perPage = 3,
                    support = Support("tes", "url"),
                    totalPages=3,
                    total = 4
                )
            )
        )

        getUsersUseCase().onEach {
            assertEquals("amit", it.data!![0].firstName)
        }
    }

    @Test
    fun returnErrorFromGetUsersUseCase() = runTest {
        `when`(userRepository.getUsers()).thenThrow(
            RuntimeException("Something Went Wrong")
        )
        getUsersUseCase().onEach {
            assertEquals("Something Went Wrong", it.message)
        }
    }
}