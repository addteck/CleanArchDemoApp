package com.addtech.wiprodemoapp.presenter.user_details

import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.data.remote.UserApi
import com.addtech.wiprodemoapp.data.remote.dto.Support
import com.addtech.wiprodemoapp.data.remote.dto.UserDetailDto
import com.addtech.wiprodemoapp.data.remote.dto.UserDto
import com.addtech.wiprodemoapp.data.repository.UserRepoImpl
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import retrofit2.Response.error

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetUserDetailsRepoTest {

    @get:Rule
    val mainCoroutineRule = MainDispatcherRule()
    private lateinit var userRepoImpl: UserRepoImpl
    private val userApi: UserApi =mock()

    @Before
    fun setUp(){

        userRepoImpl = UserRepoImpl(userApi)
    }

    @Test
    fun givenSuccessWhenUserDetailsFetchedFromBackend() = runTest{
        Mockito.`when`(userApi.getUserDetails("1")).thenReturn(
            Response.success(UserDetailDto(data = UserDto(
                avatar = "image_url",
                firstName = "amit",
                id = 1,
                lastName = "Den",
                email = "test.com"
            ),
                support = Support("tes","url")

            ))
        )
        val result = userRepoImpl.getUser("1")
        mainCoroutineRule.testDispatcher.scheduler.advanceUntilIdle()
        assertEquals("amit",result.data!!.data.firstName)
    }
    @Test
    fun givenErrorWhenUserDetailsFetchedFromBackend() = runTest{
        Mockito.`when`(userApi.getUserDetails("1")).thenReturn(
            error(500, "Something went wrong".toResponseBody("text/plain".toMediaTypeOrNull()))
        )

        val result = userRepoImpl.getUser("1")
        mainCoroutineRule.testDispatcher.scheduler.advanceUntilIdle()
        assertEquals("Something went wrong",result.message)
    }
}