package com.addtech.wiprodemoapp.presenter.user_details

import androidx.lifecycle.SavedStateHandle
import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.core.common.Constants
import com.addtech.wiprodemoapp.core.utils.Resource
import com.addtech.wiprodemoapp.domain.model.User
import com.addtech.wiprodemoapp.domain.usecase.GetUserDetailsUseCase
import com.addtech.wiprodemoapp.presenter.ui.UiState
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class UserDetailViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val getUserDetailsUseCase: GetUserDetailsUseCase = mock()


    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setup() {
        savedStateHandle = SavedStateHandle().apply {
            set(Constants.PARAM_USER_ID, "1")
        }


    }

    @Test
    fun givenResponseLoadingWhenGetUserDetailsUseCaseCalled() = runTest {

        `when`(getUserDetailsUseCase("1")).thenReturn(
            flow {
                emit(Resource.Loading())
            }
        )
        val userListViewModel = UserDetailViewModel(getUserDetailsUseCase, savedStateHandle)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Loading, userListViewModel.uiState.value)
    }

    @Test
    fun givenResponseSuccessWhenGetUserDetailsUseCaseCalled() = runTest {

        `when`(getUserDetailsUseCase("1")).thenReturn(
            flow {
                emit(
                    Resource.Success(
                        data = User(
                            avatar = "image_url",
                            firstName = "amit",
                            id = 1,
                            lastName = "Den"
                        )
                    )
                )
            }
        )
        val userListViewModel = UserDetailViewModel(getUserDetailsUseCase, savedStateHandle)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Success<User>(data = User(
            avatar = "image_url",
            firstName = "amit",
            id = 1,
            lastName = "Den"
        )), userListViewModel.uiState.value)
    }


    @Test
    fun givenResponseFailureWhenGetUserDetailsUseCaseCalled() = runTest {

        `when`(getUserDetailsUseCase("1")).thenReturn(
            flow {
                emit(Resource.Error(message = "something went wrong"))
            }
        )
        val userListViewModel = UserDetailViewModel(getUserDetailsUseCase, savedStateHandle)
        mainDispatcherRule.testDispatcher.scheduler.advanceUntilIdle()

        assertEquals(UiState.Error("something went wrong"), userListViewModel.uiState.value)
    }

}