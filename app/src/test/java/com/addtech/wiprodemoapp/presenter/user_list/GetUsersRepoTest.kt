package com.addtech.wiprodemoapp.presenter.user_list

import com.addtech.wiprodemoapp.MainDispatcherRule
import com.addtech.wiprodemoapp.data.remote.UserApi
import com.addtech.wiprodemoapp.data.remote.dto.Support
import com.addtech.wiprodemoapp.data.remote.dto.UserDto
import com.addtech.wiprodemoapp.data.remote.dto.UserResponse
import com.addtech.wiprodemoapp.data.repository.UserRepoImpl
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import retrofit2.Response.error

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetUsersRepoTest {

    @get:Rule
    val mainCoroutineRule = MainDispatcherRule()
    private lateinit var userRepoImpl: UserRepoImpl
    private val userApi: UserApi =mock()

    @Before
    fun setUp(){

        userRepoImpl = UserRepoImpl(userApi)
    }

    @Test
    fun givenSuccessWhenUsersFetchedFromBackend() = runTest{
        Mockito.`when`(userApi.getUsers()).thenReturn(
            Response.success( UserResponse(
                    users= listOf(UserDto(
                        avatar = "image_url",
                        firstName = "amit",
                        id = 1,
                        lastName = "Den",
                        email = "test.com"
                    )),
                    page=1,
                    perPage = 3,
                    support = Support("tes", "url"),
                    totalPages=3,
                    total = 4
                )
            )
        )
        val result = userRepoImpl.getUsers()
        mainCoroutineRule.testDispatcher.scheduler.advanceUntilIdle()
        assertEquals("amit",result.data!!.users[0].firstName)
    }
    @Test
    fun givenErrorWhenUsersFetchedFromBackend() = runTest{
        Mockito.`when`(userApi.getUsers()).thenReturn(
            error(500, "Something went wrong".toResponseBody("text/plain".toMediaTypeOrNull()))
        )

        val result = userRepoImpl.getUsers()
        mainCoroutineRule.testDispatcher.scheduler.advanceUntilIdle()
        assertEquals("Something went wrong",result.message)
    }
}