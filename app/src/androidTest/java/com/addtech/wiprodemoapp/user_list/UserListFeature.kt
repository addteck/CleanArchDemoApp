package com.addtech.wiprodemoapp.user_list

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.addtech.wiprodemoapp.presenter.MainActivity
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test

class UserListFeature {


    @get:Rule
    val composeRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun validateProgressBarVisible() {
        composeRule.apply {
            onNodeWithTag("progress").assertIsDisplayed()
        }
    }


    @Test
    fun validateIsUserListVisible() = runTest {
        composeRule.apply {
            Thread.sleep(2000)
            onNodeWithTag("user_list").assertIsDisplayed()
        }
    }

    @Test
    fun validateNavigationFromUserListToUserDetails() {
        composeRule.apply {
            Thread.sleep(2000)
            onNodeWithTag("user_list").onChildAt(0).performClick()
        }
    }
}