package com.addtech.wiprodemoapp.user_details

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.addtech.wiprodemoapp.presenter.MainActivity
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserDetailsFeature {


    @get:Rule
    val composeRule = createAndroidComposeRule<MainActivity>()


    @Test
    fun validateUserDetailsImageVisible() {
        runTest {
            composeRule.apply {
                Thread.sleep(4000)
                onNodeWithTag("user_list").onChildAt(0).performClick()
                onNodeWithTag("user_image").assertIsDisplayed()
            }
        }

    }

    @Test
    fun validateUserDetailsVisible() {
        composeRule.apply {
            Thread.sleep(4000)
            onNodeWithTag("user_list").onChildAt(0).performClick()
            onNodeWithTag("user_details").assertIsDisplayed()
        }
    }

}